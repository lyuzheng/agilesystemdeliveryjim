
import java.sql.SQLException;

import java.text.ParseException;

public class Subscription {
	private String dateStart;
	private String dateEnd;
	private Database myDB = null;
	private boolean independent;
	private boolean mirror;
	private boolean leader;
	private boolean topic;
	
	public Subscription(String ds, String de, boolean independent, boolean mirror, boolean leader, boolean topic, Database d) {
		this.dateStart = ds;
		this.dateEnd = de;
		this.independent = independent;
		this.mirror = mirror;
		this.leader = leader;
		this.topic = topic;
		myDB = d;
	}

	public boolean insertSubscription() 
	{
		boolean insert = false;
		try 
		{
			insert = myDB.addNewSubscription(dateStart, dateEnd, independent, mirror, leader, topic);
		}
		catch (SQLException | ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return insert;
	}

}
